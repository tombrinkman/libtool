
INSTALL_LOCATION=~/install

.c.o :
	cc -I . -c $<

hello-world-c: hello-world.o libhello_world.so
	cc -o $@ hello-world.o -L. -lhello_world 

hello-world2-c: hello-world2.o libhello_world.so
	cc -o $@ hello-world2.o -L. -lhello_world 

hello-world2-a: hello-world2.o libhello_world.a
	cc -o $@ hello-world2.o -L. -lhello_world 

libhello_world.a: 
	cc -c libhello_world.c -o libhello_world.o
	ar  rcs libhello_world.a     libhello_world.o

libhello_world.so: libhello_world.c libhello_world.h
	cc -o $@ -shared libhello_world.c -fpic

libhello_world.exe: libhello_world.c libhello_world.h
	libtool --mode=link gcc -g -O -o hell main.o libhello.la
	gcc -g -O -o .libs/hell main.o -L./.libs -R/usr/local/lib -lhello -lm
	
hello-world-install:
	mkdir -p $(INSTALL_LOCATION)/bin
	cp hello-world-c $(INSTALL_LOCATION)/bin
	chmod 755 $(INSTALL_LOCATION)/bin/hello-world-c
	cp hello-world2-c $(INSTALL_LOCATION)/bin
	chmod 755 $(INSTALL_LOCATION)/bin/hello-world2-c
	mkdir -p $(INSTALL_LOCATION)/lib
	cp libhello_world.so $(INSTALL_LOCATION)/lib
	mkdir -p $(INSTALL_LOCATION)/include
	cp libhello_world.h $(INSTALL_LOCATION)/include

clean:
	rm -f *.o hello-world-c hello-world2-c libhello_world.so libhello_world2.so libhello_world2.a
