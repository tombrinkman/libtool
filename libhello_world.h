#ifdef LIBHELLO_WORLD_H
#define LIBHELLO_WORLD_H 1
//https://www.cs.swarthmore.edu/~newhall/unixhelp/howto_C_libraries.html
//https://www.gnu.org/software/libtool/manual/html_node/Linking-executables.html
//https://en.wikipedia.org/wiki/Executable_and_Linkable_Format

int print_hello_world();

#endif
